const initialState ={
    age : 50,
    history:[],
    username : "",
    password : "",
    // logFormSubmit : false,
     userNameError : "",
     passwordError : "",
     loginStatus : "false",
     loginStatusFlag : "true"
}

const reducer = (state = initialState ,action) =>{
     const newState = {...state};
    
     // eslint-disable-next-line default-case
     console.log(action);
     // eslint-disable-next-line default-case
     switch(action.type){
         case "age_up":
         //newState.age = newState.age+1;
         return {
            ...state,
            age : state.age + action.value,
            history : state.history.concat({
                id: Math.random(),
                age : state.age + action.value})
         }
         break;
         case "age_down":
         //newState.age = newState.age-1;
         return {
            ...state,
            age : state.age - action.value,
            history : state.history.concat({
                id: Math.random(),
                age : state.age - action.value 
            })
         }
         break;
         case "DeleteItem":
         //newState.age = newState.age-1;
         return {
            ...state,
             history : state.history.filter(el => el.id !== action.key)
         }
         break;
         case "username":
         return {
            ...state,
            username : action.val
         }
         break;
         case "password":
         return {
            ...state,
            password : action.val
         }
         break;
     }
     return newState;
}

export default reducer;