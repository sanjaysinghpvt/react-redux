import React, { Component } from 'react';

import { connect } from 'react-redux';
import { userActions } from '../actions';


class LoginView extends Component {
  constructor(props) {
    super(props);
    this.state = {
       logFormSubmit: false,
       username:"",
       password:"",
       userNameError: "",
       passwordError: ""
    };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleSubmit(e) {
    e.preventDefault();

    this.setState({ logFormSubmit: true });
    const { username, password } = this.state;
    if (username && password) {
      this.props.login(username, password);
  }
}

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
}
  render() {
    const { username, password, logFormSubmit } = this.state;
    return (
      <React.Fragment>
        <div className="preloader">
          <div className="loader">
            <div className="loader__figure"></div>
            <p className="loader__label">Elegant admin</p>
          </div>
        </div>
        <section id="wrapper" className="login-register login-sidebar" >
          <div className="login-box card">
            <div className="card-body">
              <form className="form-horizontal form-material" id="loginform" onSubmit={this.handleSubmit}>
                <a href="javascript:void(0)" className="text-center db"><img src="../assets/images/logo-icon.png" alt="Home" /><br /><img src="../assets/images/logo-text.png" alt="Home" /></a>
                <div className="form-group m-t-40">
                  <div className="col-xs-12">
                    <input className="form-control" type="text" required="" value={username} name="username" placeholder="Username" onChange={this.handleChange}  />
                    {logFormSubmit && !username &&
                            <div className="help-block">Username is required</div>
                        }
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-xs-12">
                    <input className="form-control" type="password" required="" value={password} name="password" placeholder="Password" onChange={this.handleChange}  />
                    {logFormSubmit && !password &&
                            <div className="help-block">Username is required</div>
                        }
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-md-12">
                    <div className="custom-control custom-checkbox">
                      <input type="checkbox" className="custom-control-input" id="customCheck1" />
                      <label className="custom-control-label" for="customCheck1">Remember me</label>
                      <a href="javascript:void(0)" id="to-recover" className="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a>
                    </div>
                  </div>
                </div>
                <div className="form-group text-center m-t-20">
                  <div className="col-xs-12">
                    <button className="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Log In</button>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                    <div className="social"><a href="javascript:void(0)" className="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
                  </div>
                </div>
                <div className="form-group m-b-0">
                  <div className="col-sm-12 text-center">
                    Don't have an account? <a href="pages-register2.html" className="text-primary m-l-5"><b>Sign Up</b></a>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    test: state.test
  }

}
const mapDispachToProps = (dispach) => {
  return {
    // onAgeUp: () => dispach({ type: 'age_up' }),
    // onAgeDown: () => dispach({ type: 'age_down' }),
    // login: () => dispach({type: 'login'}) ,
    // logout: () => dispach({type: 'logout'}) ,
    login: userActions.login,
    //handleChange: (e) => dispach({type:e.target.name,val:e.target.value}),
  }
}
export default connect(mapStateToProps, mapDispachToProps)(LoginView);