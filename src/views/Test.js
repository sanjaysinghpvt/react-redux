// import React, { Component } from 'react';

// import { connect } from 'react-redux';
// import * as actionCreater from '../store/actions/actions.js';
// //import reducer from './store/reducer/reducer';

// class Test extends Component {

//   render() {
//     return (
//       <React.Fragment>
//         <div>age: <span>{this.props.age}</span></div>
//         <button onClick={this.props.onAgeUp}>up</button>
//         <button onClick={this.props.onAgeDown}>onAgeDown</button>
//         <div>History</div>
//         <ul>
//           {
//             this.props.history.map(el => (
//               <li key={el.id} onClick={() => this.props.DeleteItem(el.id)}>
//                 {el.age}
//               </li>
//             ))
//           }
//         </ul>
//       </React.Fragment>
//     );
//   }
// }

// const mapStateToProps = (state) => {
//   return {
//     age: state.age,
//     history: state.history
//   }

// }
// const mapDispachToProps = (dispach) => {
//   return {
//     onAgeUp: () => dispach(actionCreater.ageUp(1)),
//     onAgeDown: () => dispach(actionCreater.ageDown(1)),
//     DeleteItem: (id) => dispach({ type: 'DeleteItem', key: id }),
//   }
// }
// export default connect(mapStateToProps, mapDispachToProps)(Test);