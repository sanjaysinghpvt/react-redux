import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
//import Route from 'react-router-dom/Route'
import LoginView  from "./views/LoginView";
import Test  from "./views/Test";

class App extends Component {
  render(){
    return(
      <Router>
      <div className="App">
        <Route path="/" exact strict render={
          ()=>{
            return ("sanjay");
          }
        } />
        <Route path="/log-in" exact component={ LoginView } />
        <Route path="/test" exact component={ Test } />
      </div>
      </Router>
    );
  }
}
export default (App);
